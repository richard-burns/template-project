import os

class Utils:
	"""
	Contains all our project management classes.
	"""
	def __init__(self, ownerComp):
		self.ownerComp = ownerComp

	def hello(self):
		print("woo")
		
	def saveScripts(self):
		if not os.path.exists(str(parent.project.par.Scriptsfolder)):
			os.makedirs(str(parent.project.par.Scriptsfolder))
			
		for s in parent.project.findChildren():
			if s.family == "DAT":
				if s.type == "text":
					filename = str(parent.project.par.Scriptsfolder) + s.path + ".py"
					os.makedirs(os.path.dirname(filename), exist_ok=True)
					s.save(filename)
					s.par.file = filename
					s.par.loadonstart = 1
					s.par.write = 1
					
	def saveToxs(self):
		if not os.path.exists(str(parent.project.par.Toxfolder)):
			os.makedirs(str(parent.project.par.Toxfolder))

		for c in parent.project.findChildren():
			if c.family == "COMP":
				filename = str(parent.project.par.Toxfolder) + c.path + ".tox"
				os.makedirs(os.path.dirname(filename), exist_ok=True)
				c.save(filename)
				c.par.externaltox = filename
				
				