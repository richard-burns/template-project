# me - this DAT
# 
# dat - the DAT that received the key event
# key - the name of the key attached to the event
# character - the ASCII value of the pressed key as a string
# alt - True if the alt modifier is pressed
# ctrl - True if the ctrl modifier is pressed
# shift - True if the shift modifier is pressed
# state - True if the event is a key press event
# time - the time when the event came in milliseconds

def keyEvent(dat, key, character, alt, lAlt, rAlt, ctrl, lCtrl, rCtrl, shift, lShift, rShift, state, time):
	return

# shortcutName is the name of the shortcut

def shortcutEvent(dat, shortcutName, time):
	return;
	